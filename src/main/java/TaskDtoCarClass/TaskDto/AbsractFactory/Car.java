package TaskDtoCarClass.TaskDto.AbsractFactory;

public interface Car {

    String getModel();
    String getEngine();
    String getVinCode();
    String getColor();
}
