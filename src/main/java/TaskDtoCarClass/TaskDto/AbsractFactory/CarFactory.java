package TaskDtoCarClass.TaskDto.AbsractFactory;

public interface CarFactory {
    Car getCar(String model,String engine,String vincode,String color);
}
