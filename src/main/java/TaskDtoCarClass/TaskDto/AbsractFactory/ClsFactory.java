package TaskDtoCarClass.TaskDto.AbsractFactory;

public class ClsFactory implements CarFactory{
    @Override
    public Car getCar(String model, String engine, String vincode, String color) {
        return new Cls(color,vincode,engine,model);
    }
}
