package TaskDtoCarClass.TaskDto.AbsractFactory;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class SLS implements Car {

    private  String color;
    private  String VinCode;
    private  String engine;
    private  String model;
    @Override
    public String getModel() {
        return null;
    }

    @Override
    public String getEngine() {
        return null;
    }

    @Override
    public String getVinCode() {
        return null;
    }

    @Override
    public String getColor() {
        return null;
    }
}
