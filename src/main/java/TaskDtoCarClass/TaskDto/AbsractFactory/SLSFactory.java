package TaskDtoCarClass.TaskDto.AbsractFactory;

public class SLSFactory implements CarFactory{
    @Override
    public Car getCar(String model, String engine, String vincode, String color) {
        return new SLS(color,vincode,engine,model);
    }
}
