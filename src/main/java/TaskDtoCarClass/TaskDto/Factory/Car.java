package TaskDtoCarClass.TaskDto.Factory;

public interface Car {

    String getModel();
    String getEngine();
    String getVinCode();
    String getColor();
}
