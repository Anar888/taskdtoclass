package TaskDtoCarClass.TaskDto.Factory;

public class CarFactory {
    public static Car getCar(String model,String engine,String vincode,String color){
        Car car;
        if("Cls".equalsIgnoreCase(model)){
            car=new Cls(color,vincode,engine,model);
        } else if ("SLS".equalsIgnoreCase(model)) {
            car=new SLS(color,vincode,engine,model);
        }
        else{
            throw new RuntimeException("bele model yoxdur");
        }
        return car;
    }
}
