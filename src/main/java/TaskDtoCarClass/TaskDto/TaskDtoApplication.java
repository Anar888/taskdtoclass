package TaskDtoCarClass.TaskDto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TaskDtoApplication {

	public static void main(String[] args) {
		SpringApplication.run(TaskDtoApplication.class, args);
	}

}
