package TaskDtoCarClass.TaskDto.controller;

import TaskDtoCarClass.TaskDto.dto.CarDto;
import TaskDtoCarClass.TaskDto.dto.CreateCarDto;
import TaskDtoCarClass.TaskDto.dto.UpdateCarDto;
import TaskDtoCarClass.TaskDto.repository.CarRepository;
import TaskDtoCarClass.TaskDto.service.CarService;
import jakarta.persistence.criteria.CriteriaBuilder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/car")
public class CarController {
    private final CarService carService;
    public CarController(CarService carService){

        this.carService=carService;
    }

    @PostMapping
    public void create(@RequestBody CreateCarDto dto){

        carService.create(dto);
    }
    @PutMapping
    public void update(@RequestBody UpdateCarDto dto){
        carService.update(dto);

    }
    @GetMapping("{id}")
    public CarDto get(@PathVariable Integer id){

        return carService.get(id);
    }
    @DeleteMapping("{id}")
    public void delete(@PathVariable Integer id){

        carService.delete(id);
    }
    @GetMapping
    public List<CarDto> getAll(){
        return carService.getAll();
    }
}
