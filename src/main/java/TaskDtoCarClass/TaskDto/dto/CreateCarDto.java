package TaskDtoCarClass.TaskDto.dto;

import lombok.Data;

import java.time.LocalDate;
@Data
public class CreateCarDto {
    private String color;
    private double engine;
    private String model;
    private String maker;
    private LocalDate localdate;
}
