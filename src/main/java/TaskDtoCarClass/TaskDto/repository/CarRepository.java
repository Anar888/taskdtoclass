package TaskDtoCarClass.TaskDto.repository;

import TaskDtoCarClass.TaskDto.model.Car;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CarRepository extends JpaRepository<Car,Integer> {
}
