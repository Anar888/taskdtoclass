package TaskDtoCarClass.TaskDto.service;

import TaskDtoCarClass.TaskDto.dto.CarDto;
import TaskDtoCarClass.TaskDto.dto.CreateCarDto;
import TaskDtoCarClass.TaskDto.dto.UpdateCarDto;
import TaskDtoCarClass.TaskDto.model.Car;
import TaskDtoCarClass.TaskDto.repository.CarRepository;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.sql.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Primary
public class CarService {
    public final CarRepository carRepository;
    public final ModelMapper modelMapper;
    public CarService(CarRepository carRepository,ModelMapper modelMapper){
        this.carRepository=carRepository;
        this.modelMapper=modelMapper;
    }
    public void create(CreateCarDto createCarDto) {
       Car car=modelMapper.map(createCarDto,Car.class);
       carRepository.save(car);
    }

    public void update(UpdateCarDto dto) {
      Optional<Car> entity= carRepository.findById(dto.getId());
      entity.ifPresent(car -> {

                 car.setEngine(dto.getEngine());
                 car.setColor(dto.getColor());
                 car.setLocaldate(dto.getLocaldate());
                 car.setMaker(dto.getMaker());
                 car.setModel(dto.getModel());
                 carRepository.save(car);
      });

    }


    public CarDto get(Integer id) {
        Car car=carRepository.findById(id).get();
        CarDto carDto=modelMapper.map(car,CarDto.class);
        return carDto;

    }

    public void delete(Integer id) {

        carRepository.deleteById(id);
    }

    public List<CarDto> getAll() {
    List<Car> cars=carRepository.findAll();
        List<CarDto> carsdto= new ArrayList<>();

        cars.forEach(car ->
                carsdto.add(modelMapper.map(car, CarDto.class)));
        return carsdto;


    }

}
